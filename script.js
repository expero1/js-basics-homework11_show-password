/*
  Написати реалізацію кнопки "Показати пароль". 
  Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

Технічні вимоги:

- У файлі index.html лежить розмітка двох полів вводу пароля.
- Після натискання на іконку поруч із конкретним полем - повинні 
  відображатися символи, які ввів користувач, іконка змінює свій зовнішній вигляд. 
  У коментарях під іконкою - інша іконка, саме вона повинна відображатися замість поточної.
- Коли пароля не видно - іконка поля має виглядати як та, що в першому полі (Ввести пароль)
- Коли натиснута іконка, вона має виглядати, як та, що у другому полі (Ввести пароль)
- Натиснувши кнопку Підтвердити, потрібно порівняти введені значення в полях
- Якщо значення збігаються – вивести модальне вікно (можна alert) з текстом – You are welcome;
- Якщо значення не збігаються - вивести під другим полем текст червоного кольору Потрібно ввести однакові значення

Після натискання на кнопку сторінка не повинна перезавантажуватись
Можна міняти розмітку, додавати атрибути, теги, id, класи тощо.
*/

"use strict";
const modalWrapper = document.querySelector(".modal-wrapper");
modalWrapper.addEventListener("click", ({ target }) => {
  if (!target.closest(".modal-window") || target.closest(".modal-close-btn"))
    modalWrapper.classList.remove("active");
});
function showModalMessage(message) {
  modalWrapper.classList.add("active");
  modalWrapper.querySelector(".modal-content").innerText = message;
}

const form = document.querySelector(".password-form");
form.addEventListener("click", (event) => {
  togglePasswordVisibility(event);
  submitForm(event);
});

function togglePasswordVisibility(event) {
  event.preventDefault();
  const showPasswordIcon = event.target.closest(".icon-password");
  if (!showPasswordIcon) return;
  const input = showPasswordIcon.closest("label").querySelector("input");
  input.classList.toggle("show");
  input.type = input.classList.contains("show") ? "text" : "password";
}

function submitForm(event) {
  if (event.target.closest("form button[type='submit']")) checkForm(event);
}
function checkForm(event) {
  event.preventDefault();
  const emptyPasswordMessage = "Please enter password";
  const notEqualMessage = "Потрібно ввести однакові значення";
  const password = event.currentTarget.querySelector("input#password");
  const confirmPassword = event.currentTarget.querySelector(
    "input#confirm-password"
  );
  const showHelpText = (input, message = "") => {
    const helpText = input.closest("label").querySelector(".help-text");
    message === ""
      ? (helpText.innerHTML = "")
      : helpText.insertAdjacentHTML("beforeend", `<li>${message}</li>`);
  };
  for (let input of [password, confirmPassword]) {
    showHelpText(input);
    input.classList.remove("invalid");
  }

  const empty = (value) => value.length === 0;
  const notEqual = (value, comparedValue) =>
    value.length > 0 && comparedValue.length > 0 && value !== comparedValue;
  let valid = true;

  if (empty(password.value)) {
    password.classList.add("invalid");
    showHelpText(password, emptyPasswordMessage);
    valid = false;
  }
  if (empty(confirmPassword.value)) {
    confirmPassword.classList.add("invalid");
    showHelpText(confirmPassword, emptyPasswordMessage);
    valid = false;
  }
  if (notEqual(password.value, confirmPassword.value)) {
    password.classList.add("invalid");
    confirmPassword.classList.add("invalid");
    showHelpText(confirmPassword, notEqualMessage);
    valid = false;
  }
  if (valid) showModalMessage("You are welcome");
}
